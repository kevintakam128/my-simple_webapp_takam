
FROM node:18-alpine


WORKDIR /app


COPY package*.json ./




COPY . .

# Expose the port that your application listens on
EXPOSE 8080

# Define the command to run your application
CMD ["node", "src/count-server.js"]
